DROP DATABASE `atp_dwh`;

CREATE DATABASE `atp_dwh`;

USE `atp_dwh`;

CREATE USER IF NOT EXISTS `admin_dwh`@`localhost` IDENTIFIED BY 'admin';
-- GRANT USAGE ON *.* TO `admin_dwh`@`localhost` REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
GRANT ALL PRIVILEGES ON `atp_dwh`.* TO `admin_dwh`@`localhost` WITH GRANT OPTION;

DROP TABLE IF EXISTS `dim_tmp`;

CREATE TABLE `dim_tmp`(
    `dim_tmp_id` int(10)  AUTO_INCREMENT,
    `tmp_last_update` timestamp,
    `tmp_value` date,
    `tmp_short` VARCHAR(12),
    `tmp_full` VARCHAR(100),
    `day_in_month` TINYINT(3),
    `day_name` VARCHAR(12),
    `trimestre` varchar(1),
    `week_in_year` TINYINT(3),
    `month_number` TINYINT(3),
    `month_name` VARCHAR(12),
    `year4` SMALLINT(5),

    PRIMARY KEY (`dim_tmp_id`)
)ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `dim_apoyo_economico`;

CREATE TABLE `dim_apoyo_economico`(
    `dim_eco_id` int(10) AUTO_INCREMENT,
    `eco_last_update` datetime,
    `eco_importe_anual` FLOAT(10,2),
    `eco_cif` INT(10),
    `eco_sector` VARCHAR(30),
    `eco_localidad` VARCHAR(50),
    `eco_pais` VARCHAR(50),
    `eco_poblacion` int(10),
    `eco_valid_from` date,
    `eco_valid_through` date,
    `eco_version_number` SMALLINT(3),
   
    `eco_nombre_empresa` VARCHAR(50),

    PRIMARY KEY (`dim_eco_id`)

)ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `dim_apoyo_profesional`;

CREATE TABLE `dim_apoyo_profesional`(
    `dim_pro_id` int(10) AUTO_INCREMENT,
    `pro_entrenador_id` int(10),
    `pro_last_update` datetime,
    `pro_nombre_ent` VARCHAR(45),
    `pro_apellidos_ent` VARCHAR(60),
    `pro_fecha_nac` date,
    `pro_t_activo` int(3),
    `pro_t_jugador` int(3),
    `pro_estudios` VARCHAR(200),

    PRIMARY KEY (`dim_pro_id`)

)ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `dim_tenista`;

create table `dim_tenista` (
    `dim_tenista_id` int(10) AUTO_INCREMENT,
    `tenista_num_atp` INT(10),
    `tenista_last_update` date,
    `tenista_nombre` char(20),
    `tenista_apellido` char(50),
    `tenista_lesiones` smallint(5),
    `tenista_fecha_nacimiento` DATE,
    `tenista_pais` VARCHAR(50),
    `tenista_sexo` ENUM('HOMBRE', 'MUJER'),
    `tenista_valid_through` date,
    `tenista_valid_from` date,
    `tenista_version_number` SMALLINT(3),

    PRIMARY KEY (`dim_tenista_id`)

)ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `dim_localizacion`;

CREATE TABLE `dim_localizacion` (
    `dim_local_id` int(10) AUTO_INCREMENT,
    `local_torneo_id` INT(10),
    `local_pista_id` INT(10),
    `local_last_update` TIMESTAMP,
    `local_pais` CHAR(50),
    `local_poblacion` int(7),
    `local_tipo_terreno` char(50),
    `local_tipo_torneo` VARCHAR(50),
    `local_tipo_dia` char(60),
    `local_temperatura` int(3),
    `local_humedad` int(3),

    PRIMARY KEY(`dim_local_id`)

)ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `fact_rtoTenista`;

CREATE TABLE `fact_rtoTenista`(
    
    `rto_id` int(10) AUTO_INCREMENT,
    `rto_last_update` TIMESTAMP,
    `dim_local_id` int(10),
    `dim_tenista_id` int(10),
    `dim_eco_id` int(10),
    `dim_pro_id` int(10),
    `dim_tmp_id` int(10),
    `porcentaje_part_ganado` int(3),
    `resultado` int(10),
    `faltas` int(3),
    `id_adversario` int(10),

    PRIMARY KEY (`rto_id`),
    FOREIGN KEY (`dim_local_id`) REFERENCES dim_localizacion (`dim_local_id`),
    FOREIGN KEY (`dim_tenista_id`) REFERENCES dim_tenista (`dim_tenista_id`),
    FOREIGN KEY (`dim_eco_id`) REFERENCES dim_apoyo_economico (`dim_eco_id`),
    FOREIGN KEY (`dim_pro_id`) REFERENCES dim_apoyo_profesional (`dim_pro_id`),
    FOREIGN KEY (`dim_tmp_id`) REFERENCES dim_tmp (`dim_tmp_id`)

)ENGINE=MyISAM AUTO_INCREMENT=201 DEFAULT CHARSET=latin1;